package ru.fnight.moneyrest.repository;

import org.springframework.data.repository.CrudRepository;

import ru.fnight.moneyrest.model.Bank;

public interface BankRepository extends CrudRepository<Bank, Long> {
	public Bank findOneByName(String name);
	public Bank findOneByBic(String bic);
}
package ru.fnight.moneyrest.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.beans.factory.annotation.Autowired;

import ru.fnight.moneyrest.model.Deposit;
import ru.fnight.moneyrest.repository.DepositRepository;

@RestController
@RequestMapping("/deposit")
public class DepositController {
    
    @Autowired
    private DepositRepository repository;

    @PostMapping("/")
    public Deposit createDeposit(@ModelAttribute Deposit deposit) {
		return repository.save(deposit);
    }
    
    @GetMapping("/")
    public Iterable<Deposit> allDeposits() {
		return repository.findAll();
    }
    
    @GetMapping("/{id}")
    public Deposit oneDepositById(@PathVariable("id") long id) {
        return repository.findOne(id);
    }
    
    @DeleteMapping("/{id}")
    public String deleteDepositById(@PathVariable("id") long id) {
        repository.delete(repository.findOne(id));
        return "Deposit was deleted";
    }
    
    @PutMapping("/{id}")
    public Deposit updateDeposit(@ModelAttribute Deposit newDeposit, @PathVariable long id) {
        Deposit deposit = repository.findOne(id);
        deposit.setIdClient(newDeposit.getIdClient());
        deposit.setIdBank(newDeposit.getIdBank());
        deposit.setDate(newDeposit.getDate());
        deposit.setProcent(newDeposit.getProcent());
        deposit.setPeriod(newDeposit.getPeriod());
        return repository.save(deposit);
    }
}
package ru.fnight.moneyrest.repository;

import org.springframework.data.repository.CrudRepository;

import ru.fnight.moneyrest.model.Deposit;

public interface DepositRepository extends CrudRepository<Deposit, Long> {
	public Deposit findOneByIdClient(int idClient);
	public Deposit findOneByIdBank(int idBank);
	public Deposit findOneByDate(int date);
	public Deposit findOneByProcent(int procent);
	public Deposit findOneByPeriod(int period);

}
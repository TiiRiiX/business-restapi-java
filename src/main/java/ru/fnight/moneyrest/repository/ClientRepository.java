package ru.fnight.moneyrest.repository;

import org.springframework.data.repository.CrudRepository;

import ru.fnight.moneyrest.model.Client;

public interface ClientRepository extends CrudRepository<Client, Long> {
	public Client findOneByFullName(String fullName);
	public Client findOneByShortName(String shortName);
	public Client findOneByAddress(String address);
	public Client findOneByForm(int form);
}
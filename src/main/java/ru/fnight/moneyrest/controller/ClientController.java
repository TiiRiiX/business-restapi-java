package ru.fnight.moneyrest.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.beans.factory.annotation.Autowired;

import ru.fnight.moneyrest.model.Client;
import ru.fnight.moneyrest.repository.ClientRepository;

@RestController
@RequestMapping("/client")
public class ClientController {
    
    @Autowired
    private ClientRepository repository;

    @PostMapping("/")
    public Client createClient(@ModelAttribute Client client) {
		return repository.save(client);
    }
    
    @GetMapping("/")
    public Iterable<Client> allClients() {
		return repository.findAll();
    }
    
    @DeleteMapping("/{id}")
    public String deleteClientById(@PathVariable("id") long id) {
        repository.delete(repository.findOne(id));
        return "Client was deleted";
    }
    
    @PutMapping("/{id}")
    public Client updateClient(@ModelAttribute Client newClient, @PathVariable long id) {
        Client client = repository.findOne(id);
        client.setFullName(newClient.getFullName());
        client.setShortName(newClient.getShortName());
        client.setAddress(newClient.getAddress());
        client.setForm(newClient.getForm());
        return repository.save(client);
    }
}
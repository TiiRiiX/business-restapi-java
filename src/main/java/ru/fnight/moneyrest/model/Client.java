package ru.fnight.moneyrest.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Client {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    private String fullName;
    private String shortName;
    private String address;
    private int form;
    
    protected Client() {}

    public Client(String fullName, String shortName, String address, int form) {
        this.fullName = fullName;
        this.shortName = shortName;
        this.address = address;
        this.form = form;
    }
    
    public long getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
    
    public String getShortName() {
        return shortName;
    }
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
    
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    
    public int getForm() {
        return form;
    }
    public void setForm(int form) {
        this.form = form;
    }
}